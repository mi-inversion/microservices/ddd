import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';
import { LoginResponse } from './dto/login-response.dto';
import { UserDto } from './dto/user.dto';
import { PlanDto } from './dto/plan.dto';
import { InvestPerformanceResultDto } from './dto/invest-performance-result-dto';
import { InvestPerformanceInputsDto } from './dto/invest-performance-inputs.dto';

const SERVICE_NAME = 'ddd';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern({ service: SERVICE_NAME, cmd: 'login' })
  async login(data: UserDto): Promise<LoginResponse> {
    return await this.appService.login(data.username, data.password);
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'signUp' })
  signUp(data: UserDto): Promise<LoginResponse> {
    return this.appService.signUp(data.username, data.password);
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'createPlan' })
  createPlan(data: PlanDto): Promise<LoginResponse> {
    return this.appService.createPlan(data);
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'updatePlan' })
  updatePlan(data: PlanDto): Promise<LoginResponse> {
    return this.appService.updatePlan(data);
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'getPlans' })
  getPlan(data: PlanDto): Promise<LoginResponse> {
    return this.appService.getPlans();
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'deletePlan' })
  deletePlan(data: PlanDto): Promise<LoginResponse> {
    return this.appService.deletePlan(data);
  }

  // Invest Performance

  @MessagePattern({ service: SERVICE_NAME, cmd: 'generateInvestPerformance' })
  generateInvestPerformance(
    data: InvestPerformanceInputsDto,
  ): Promise<InvestPerformanceResultDto> {
    return this.appService.generateInvestPerformance(data);
  }
}
