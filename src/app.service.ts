import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { LoginResponse } from './dto/login-response.dto';
import { PlanDto } from './dto/plan.dto';
import { InvestPerformanceInputsDto } from './dto/invest-performance-inputs.dto';

const AUTH_SERVICE = 'auth';
const PLAN_CATALOGUE_SERVICE = 'planCatalogue';
const INVEST_PERFORMANCE_SERVICE = 'investPerformance';
@Injectable()
export class AppService {
  constructor(@Inject('SERVICES') private readonly client: ClientProxy) {}

  async login(username: string, password: string): Promise<LoginResponse> {
    const pattern = { service: AUTH_SERVICE, cmd: 'login' };
    return await this.client.send(pattern, { username, password }).toPromise();
  }
  async signUp(username: string, password: string) {
    const pattern = { service: AUTH_SERVICE, cmd: 'signUp' };
    return await this.client.send(pattern, { username, password }).toPromise();
  }

  // Plans

  async createPlan(plan: PlanDto) {
    const pattern = { service: PLAN_CATALOGUE_SERVICE, cmd: 'createPlan' };
    return await this.client.send(pattern, plan).toPromise();
  }

  async getPlans() {
    const pattern = { service: PLAN_CATALOGUE_SERVICE, cmd: 'getPlans' };
    return await this.client.send(pattern, {}).toPromise();
  }

  async updatePlan(plan: PlanDto) {
    const pattern = { service: PLAN_CATALOGUE_SERVICE, cmd: 'updatePlan' };
    return await this.client.send(pattern, plan).toPromise();
  }

  async deletePlan(plan: PlanDto) {
    const pattern = { service: PLAN_CATALOGUE_SERVICE, cmd: 'deletePlan' };
    return await this.client.send(pattern, plan).toPromise();
  }

  //  InvestPerformance

  async generateInvestPerformance(plan: InvestPerformanceInputsDto) {
    const pattern = {
      service: INVEST_PERFORMANCE_SERVICE,
      cmd: 'generateInvestPerformance',
    };
    return await this.client.send(pattern, plan).toPromise();
  }
}
